package calculator;

import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

/**
 * Created by kosar on 5/6/19.
 */

public class MyStepdefs {
    public int val1;
    public int val2;
    public Calculator calculator;
    public int sum;


    @Given("^Two input values, ([-]?\\d+) and ([-]?\\d+)$")
    public void twoInputValuesAnd(int arg0, int arg1) {
        val1 = arg0;
        val2 = arg1;
    }

    @When("^I add the two values$")
    public void iAddTheTwoValues() {
        calculator = new Calculator();
        sum = calculator.Add(val1, val2);


    }

    @Then("^I expect the result ([-]?\\d+)$")
    public void iExpectTheResult(int arg0) {
        Assert.assertEquals(arg0, sum);
    }
}
